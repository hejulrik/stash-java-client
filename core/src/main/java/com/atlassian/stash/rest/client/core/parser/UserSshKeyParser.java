package com.atlassian.stash.rest.client.core.parser;

import com.atlassian.stash.rest.client.api.entity.UserSshKey;
import com.google.common.base.Function;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

class UserSshKeyParser implements Function<JsonElement, UserSshKey> {

    private static final String LABEL = "label";

    @Override
    public UserSshKey apply(final JsonElement json) {
        JsonObject jsonObject = json.getAsJsonObject();

        return new UserSshKey(
                jsonObject.get("id").getAsLong(),
                jsonObject.get("text").getAsString(),
                jsonObject.has(LABEL) ? jsonObject.get(LABEL).getAsString() : null
        );
    }

}
