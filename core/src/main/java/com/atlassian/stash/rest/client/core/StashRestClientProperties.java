package com.atlassian.stash.rest.client.core;

public class StashRestClientProperties {
    /**
     * Default page limit for requests which doesn't provide ability to set limit manually
     * (eg. {@link com.atlassian.stash.rest.client.api.StashApiService#isRepositoryKey(com.atlassian.applinks.api.ApplicationLink, String, String, String)})
     */
    public static final int STASH_REST_PAGE_LIMIT = getIntProperty(25, "stash.rest.page.limit");
    /**
     * Socket timeout in seconds
     */
    public static final int STASH_REST_SOCKET_TIMEOUT = getIntProperty(30, "stash.rest.socket.timeout");

    public static int getIntProperty(int def, String... propertyNames) {
        for (String propertyName : propertyNames) {
            Integer value = Integer.getInteger(propertyName, null);
            if (value != null) {
                return value;
            }
        }
        return def;
    }
}
