package com.atlassian.stash.rest.client.core.parser;

import com.atlassian.stash.rest.client.api.StashError;
import com.google.common.base.Function;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.List;

import static com.atlassian.stash.rest.client.core.parser.Parsers.errorParser;
import static com.atlassian.stash.rest.client.core.parser.Parsers.listParser;

public class ErrorsParser implements Function<JsonElement, List<StashError>> {
    @Override
    public List<StashError> apply(JsonElement input) {
        JsonObject jsonObject = input.getAsJsonObject();
        return listParser(errorParser()).apply(jsonObject.get("error"));
    }
}
