package com.atlassian.stash.rest.client.core;

import com.atlassian.stash.rest.client.api.StashClient;
import com.atlassian.stash.rest.client.api.StashError;
import com.atlassian.stash.rest.client.api.StashException;
import com.atlassian.stash.rest.client.api.StashRestException;
import com.atlassian.stash.rest.client.api.StashUnauthorizedRestException;
import com.atlassian.stash.rest.client.api.entity.Branch;
import com.atlassian.stash.rest.client.api.entity.Page;
import com.atlassian.stash.rest.client.api.entity.Permission;
import com.atlassian.stash.rest.client.api.entity.Project;
import com.atlassian.stash.rest.client.api.entity.Repository;
import com.atlassian.stash.rest.client.api.entity.RepositorySshKey;
import com.atlassian.stash.rest.client.api.entity.UserSshKey;
import com.atlassian.stash.rest.client.core.entity.StashRepositorySshKeyRequest;
import com.atlassian.stash.rest.client.core.entity.StashUserSshKeyRequest;
import com.atlassian.stash.rest.client.core.http.HttpExecutor;
import com.atlassian.stash.rest.client.core.http.HttpMethod;
import com.atlassian.stash.rest.client.core.http.HttpRequest;
import com.atlassian.stash.rest.client.core.http.HttpResponse;
import com.atlassian.stash.rest.client.core.http.HttpResponseProcessor;
import com.google.common.collect.ImmutableMap;
import com.google.common.io.CharStreams;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import org.apache.log4j.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

import static com.atlassian.stash.rest.client.api.StashException.toErrors;
import static com.atlassian.stash.rest.client.core.http.HttpMethod.GET;
import static com.atlassian.stash.rest.client.core.http.HttpMethod.POST;
import static com.atlassian.stash.rest.client.core.parser.Parsers.branchParser;
import static com.atlassian.stash.rest.client.core.parser.Parsers.errorsParser;
import static com.atlassian.stash.rest.client.core.parser.Parsers.pageParser;
import static com.atlassian.stash.rest.client.core.parser.Parsers.projectParser;
import static com.atlassian.stash.rest.client.core.parser.Parsers.repositoryParser;
import static com.atlassian.stash.rest.client.core.parser.Parsers.repositorySshKeyParser;
import static com.atlassian.stash.rest.client.core.parser.Parsers.userSshKeyParser;

public class StashClientImpl implements StashClient {
    private static final Logger log = Logger.getLogger(StashClientImpl.class);
    public static final int NOT_FOUND = 404;
    private final HttpExecutor httpExecutor;
    private final int pageLimit;

    /**
     * Creates client able to perform Stash REST API calls
     *
     * @param httpExecutor Http executor responsible for performing http requests
     * @param pageLimit Number of values retrieved for {@code isUserKey/isRepositoryKey} methods
     */
    public StashClientImpl(HttpExecutor httpExecutor, int pageLimit) {
        this.httpExecutor = httpExecutor;
        this.pageLimit = pageLimit;
    }

    public StashClientImpl(HttpExecutor httpExecutor) {
        this(httpExecutor, StashRestClientProperties.STASH_REST_PAGE_LIMIT);
    }

    @Nonnull
    @Override
    public Page<Project> getAccessibleProjects(final long start, final long limit) {
        String requestUrl = String.format("/rest/api/1.0/projects?start=%d", start);
        if (limit > 0) {
            requestUrl += "&limit=" + limit;
        }
        JsonElement jsonElement = doRestCall(requestUrl, GET, null, false);

        return pageParser(projectParser()).apply(jsonElement);
    }


    @Nonnull
    @Override
    public Page<Repository> getRepositories(@Nullable final String projectKey, @Nullable final String query,
                                                      final long start, final long limit) {
        String requestUrl = String.format("/rest/api/1.0/repos?start=%d", start);
        if (limit > 0) {
            requestUrl += "&limit=" + limit;
        }
        if (!isBlank(projectKey)) {
            requestUrl += "&projectname=" + encode(projectKey);
        }
        if (!isBlank(query)) {
            requestUrl += "&name=" + encode(query);
        }

        JsonElement jsonElement = doRestCall(requestUrl, GET, null, false);
        return pageParser(repositoryParser()).apply(jsonElement);
    }


    @Override
    @Nullable
    public Repository getRepository(@Nonnull final String projectKey, @Nonnull final String repositorySlug) {
        try {
            String requestUrl = String.format("/rest/api/1.0/projects/%s/repos/%s", projectKey, repositorySlug);
            JsonElement jsonElement = doRestCall(requestUrl, GET, null, false);
            return repositoryParser().apply(jsonElement);
        } catch (StashRestException e) {
            if (e.getStatusCode() == NOT_FOUND) {
                return null;
            }
            throw e;
        }
    }

    @Nonnull
    @Override
    public Page<Branch> getRepositoryBranches(@Nonnull final String projectKey, @Nonnull final String repositorySlug,
                                                        @Nullable final String query, final long start, final long limit) {
        String requestUrl = String.format("/rest/api/1.0/projects/%s/repos/%s/branches?start=%d&details=true&orderBy=MODIFICATION", projectKey, repositorySlug, start);
        if (limit > 0) {
            requestUrl += "&limit=" + limit;
        }
        if (!isBlank(query)) {
            requestUrl += "&filterText=" + encode(query);
        }

        JsonElement jsonElement = doRestCall(requestUrl, GET, null, false);
        return pageParser(branchParser()).apply(jsonElement);
    }

    @Nullable
    @Override
    public Branch getRepositoryDefaultBranch(@Nonnull final String projectKey, @Nonnull final String repositorySlug) {
        try {
            String requestUrl = String.format("/rest/api/1.0/projects/%s/repos/%s/branches/default", projectKey, repositorySlug);

            JsonElement jsonElement = doRestCall(requestUrl, GET, null, false);
            return branchParser().apply(jsonElement);
        } catch (StashRestException  e) {
            if (e.getStatusCode() == NOT_FOUND) {
                return null;
            }
            throw e;
        }
    }

    @Nonnull
    @Override
    public Page<RepositorySshKey> getRepositoryKeys(@Nonnull final String projectKey,
                                                              @Nonnull final String repositorySlug,
                                                              final long start, final long limit) {
        String requestUrl = String.format("/rest/keys/1.0/projects/%s/repos/%s/ssh?start=%d", projectKey, repositorySlug, start);
        if (limit > 0) {
            requestUrl += "&limit=" + limit;
        }

        JsonElement jsonElement = doRestCall(requestUrl, GET, null, false);
        return pageParser(repositorySshKeyParser()).apply(jsonElement);
    }

    @Override
    public boolean addRepositoryKey(@Nonnull final String projectKey, @Nonnull final String repositorySlug,
                                    @Nonnull final String publicKey, @Nullable final String keyLabel,
                                    @Nonnull final Permission keyPermission) {

        final StashRepositorySshKeyRequest payload = new StashRepositorySshKeyRequest(
                projectKey,
                repositorySlug,
                keyLabel,
                publicKey,
                keyPermission.name()
        );

        final String requestUrl = String.format("/rest/keys/1.0/projects/%s/repos/%s/ssh", projectKey, repositorySlug);
        doRestCall(requestUrl, POST, payload.toJson(), false);
        return true;
    }

    @Override
    public boolean isRepositoryKey(@Nonnull final String projectKey, @Nonnull final String repositorySlug,
                                                           @Nonnull final String publicKey) {
        if (isBlank(publicKey)) {
            return false;
        }
        Integer pageStart = 0;
        do {
            Page<RepositorySshKey> keysPage = getRepositoryKeys(projectKey, repositorySlug, pageStart, pageLimit);
            for (RepositorySshKey key : keysPage.getValues()) {
                // key already exists on the server - so as this is the same public key - we don't upload it
                if (key.getText().equals(publicKey)) {
                    return true;
                }
            }
            pageStart = keysPage.getNextPageStart();
        } while (pageStart != null);

        return false;
    }

    @Nonnull
    @Override
    public Page<UserSshKey> getCurrentUserKeys(final long start,
                                                                                                   final long limit) {
        String requestUrl = String.format("/rest/ssh/1.0/keys?start=%d", start);
        if (limit > 0) {
            requestUrl += "&limit=" + limit;
        }

        JsonElement jsonElement = doRestCall(requestUrl, GET, null, false);
        return pageParser(userSshKeyParser()).apply(jsonElement);
    }

    @Override
    public boolean isUserKey(@Nonnull final String publicKey) {
        if (isBlank(publicKey)) {
            return false;
        }

        Integer pageStart = 0;
        do {
            Page<UserSshKey> keysPage = getCurrentUserKeys(pageStart, pageLimit);
            for (UserSshKey key : keysPage.getValues()) {
                // key already exists on the server - so as this is the same public key - we don't upload it
                if (key.getText().equals(publicKey)) {
                    return true;
                }
            }
            pageStart = keysPage.getNextPageStart();
        } while (pageStart != null);

        return false;
    }

    @Override
    public boolean addUserKey(@Nonnull final String publicKey, @Nullable String keyLabel) {

        final StashUserSshKeyRequest payload = new StashUserSshKeyRequest(keyLabel, publicKey);

        final String requestUrl = String.format("/rest/ssh/1.0/keys");
        doRestCall(requestUrl, POST, payload.toJson(), false);

        return true;
    }


    @Override
    @Nonnull
    public ImmutableMap<String, String> getStashApplicationProperties() {
        String requestUrl = String.format("/rest/api/1.0/application-properties");

        JsonElement jsonElement = doRestCall(requestUrl, GET, null, true);
        final ImmutableMap.Builder<String, String> resultBuilder = ImmutableMap.builder();
        if (jsonElement != null) {
            for (final Map.Entry<String, JsonElement> entry : jsonElement.getAsJsonObject().entrySet()) {
                resultBuilder.put(entry.getKey(), entry.getValue().getAsString());
            }
        }
        return resultBuilder.build();
    }

    @Nullable
    protected JsonElement doRestCall(@Nonnull String requestUrl, @Nonnull HttpMethod methodType,
                                     @Nullable JsonObject requestJson, boolean anonymousCall) throws StashException {

        String requestData = requestJson != null ? requestJson.toString() : null;
        if (log.isTraceEnabled()) {
            log.trace(String.format("doRestCall request: methodType=%s; requestUrl='%s'; requestJson='%s'; anonymous?=%s", methodType, requestUrl, requestJson, anonymousCall));
        }

        try {
            return httpExecutor.execute(new HttpRequest(requestUrl, methodType, requestData, anonymousCall),
                    new HttpResponseProcessor<JsonElement>() {
                        @Override
                        public JsonElement process(@Nonnull HttpResponse response) throws IOException {
                            final String responseString = CharStreams.toString(new BufferedReader(response.getBodyReader("UTF-8")));
                            if (log.isTraceEnabled()) {
                                log.trace(String.format("doRestCall response: code=%d; response='%s'", response.getStatusCode(), responseString));
                            }

                            if (response.isSuccessful()) {
                                try {
                                    JsonElement jsonElement = new JsonParser().parse(responseString);
                                    return jsonElement != null && !jsonElement.isJsonNull() ? jsonElement : null;
                                } catch (JsonSyntaxException e) {
                                    throw createStashRestException(response, toErrors("Failed to parse response: " + e.getMessage()), responseString);
                                }
                            } else {
                                List<StashError> errors;
                                try {
                                    JsonElement jsonElement = new JsonParser().parse(responseString);
                                    if (jsonElement != null && jsonElement.isJsonObject()) {
                                        errors = errorsParser().apply(jsonElement);
                                    } else {
                                        errors = toErrors("Request to Stash failed. Returned with " + response.getStatusCode() + ". Response body is empty.");
                                    }
                                } catch (JsonSyntaxException entityException) {
                                    errors = toErrors("Request to Stash failed. Returned with " + response.getStatusCode());
                                }
                                throw createStashRestException(response, errors, responseString);
                            }
                        }
                    });
        } catch (RuntimeException e) {
            if (log.isTraceEnabled()) {
                log.trace(e, e);
            }
            throw e;
        }
    }

    private StashRestException createStashRestException(HttpResponse response, List<StashError> errors, String responseString) {
        switch(response.getStatusCode()) {
            case 401:
                return new StashUnauthorizedRestException(errors, response.getStatusCode(), response.getStatusMessage(), responseString);
            default:
                return new StashRestException(errors, response.getStatusCode(), response.getStatusMessage(), responseString);
        }
    }

    private static String encode(String queryString) {
        String result;
        try {
            result = URLEncoder.encode(queryString, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException("UTF-8 not supported", ex);
        }
        return result;
    }

    private static boolean isBlank(@Nullable String s) {
        return s == null || s.isEmpty() || s.trim().isEmpty();
    }

}
