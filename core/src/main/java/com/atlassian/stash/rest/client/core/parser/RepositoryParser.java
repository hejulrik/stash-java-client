package com.atlassian.stash.rest.client.core.parser;

import com.atlassian.stash.rest.client.api.entity.Project;
import com.atlassian.stash.rest.client.api.entity.Repository;
import com.atlassian.stash.rest.client.core.entity.Link;
import com.google.common.base.Function;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import javax.annotation.Nullable;
import java.util.List;

import static com.atlassian.stash.rest.client.core.parser.ParserUtil.isHttpLink;
import static com.atlassian.stash.rest.client.core.parser.ParserUtil.isSshLink;
import static com.atlassian.stash.rest.client.core.parser.ParserUtil.linkToHref;
import static com.atlassian.stash.rest.client.core.parser.Parsers.linkParser;
import static com.atlassian.stash.rest.client.core.parser.Parsers.listParser;
import static com.atlassian.stash.rest.client.core.parser.Parsers.projectParser;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.getFirst;
import static com.google.common.collect.Iterables.transform;

class RepositoryParser implements Function<JsonElement, Repository> {

    @Override
    public Repository apply(@Nullable final JsonElement json) {
        if (json == null || !json.isJsonObject()) {
            return null;
        }

        JsonObject jsonObject = json.getAsJsonObject();
        Project projectEntity = projectParser().apply(jsonObject.getAsJsonObject("project"));

        String sshCloneUrl = null;
        String httpCloneUrl = null;
        String selfUrl = null;

        if (jsonObject.has("links")) {
            JsonObject links = jsonObject.getAsJsonObject("links");

            List<Link> cloneLinks = listParser(linkParser("href", "name")).apply(links.get("clone"));
            httpCloneUrl = getFirst(transform(filter(cloneLinks, isHttpLink()), linkToHref()), httpCloneUrl);
            sshCloneUrl = getFirst(transform(filter(cloneLinks, isSshLink()), linkToHref()), sshCloneUrl);

            List<Link> selfLinks = listParser(linkParser("href", null)).apply(links.get("self"));
            selfUrl = getFirst(transform(selfLinks, linkToHref()), selfUrl);
        }

        Repository origin = jsonObject.has("origin") ? this.apply(jsonObject.get("origin")) : null;

        return new Repository(
                jsonObject.get("slug").getAsString(),
                jsonObject.get("id").getAsLong(),
                jsonObject.get("name").getAsString(),
                jsonObject.get("public").getAsBoolean(),
                sshCloneUrl,
                httpCloneUrl,
                selfUrl,
                projectEntity,
                origin
        );
    }

}
