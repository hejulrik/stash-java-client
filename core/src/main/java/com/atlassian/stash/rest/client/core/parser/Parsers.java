package com.atlassian.stash.rest.client.core.parser;

import com.atlassian.stash.rest.client.api.StashError;
import com.atlassian.stash.rest.client.api.entity.Branch;
import com.atlassian.stash.rest.client.api.entity.Page;
import com.atlassian.stash.rest.client.api.entity.Project;
import com.atlassian.stash.rest.client.api.entity.Repository;
import com.atlassian.stash.rest.client.api.entity.RepositorySshKey;
import com.atlassian.stash.rest.client.api.entity.UserSshKey;
import com.atlassian.stash.rest.client.core.entity.Link;
import com.google.common.base.Function;
import com.google.gson.JsonElement;

import java.util.List;

public class Parsers {

    public static <T> Function<JsonElement, List<T>> listParser(Function<JsonElement, T> elementParser) {
        return new ListParser<T>(elementParser);
    }

    public static Function<JsonElement, Branch> branchParser() {
        return BRANCH_PARSER;
    }

    public static Function<JsonElement, Link> linkParser(String hrefProperty, String nameProperty) {
        return new LinkParser(hrefProperty, nameProperty);
    }

    public static <T> Function<JsonElement, Page<T>> pageParser(Function<JsonElement, T> valueParser) {
        return new PageParser<T>(valueParser);
    }

    public static Function<JsonElement, Project> projectParser() {
        return PROJECT_PARSER;
    }

    public static Function<JsonElement, Repository> repositoryParser() {
        return REPOSITORY_PARSER;
    }

    public static Function<JsonElement, RepositorySshKey> repositorySshKeyParser() {
        return REPOSITORY_SSH_KEY_PARSER;
    }

    public static Function<JsonElement, UserSshKey> userSshKeyParser() {
        return USER_SSH_KEY_PARSER;
    }

    public static Function<JsonElement, StashError> errorParser() {
        return ERROR_PARSER;
    }

    public static Function<JsonElement, List<StashError>> errorsParser() {
        return ERRORS_PARSER;
    }

    private static final BranchParser BRANCH_PARSER = new BranchParser();
    private static final ProjectParser PROJECT_PARSER = new ProjectParser();
    private static final RepositoryParser REPOSITORY_PARSER = new RepositoryParser();
    private static final RepositorySshKeyParser REPOSITORY_SSH_KEY_PARSER = new RepositorySshKeyParser();
    private static final UserSshKeyParser USER_SSH_KEY_PARSER = new UserSshKeyParser();
    private static final ErrorParser ERROR_PARSER = new ErrorParser();
    private static final ErrorsParser ERRORS_PARSER = new ErrorsParser();
}
