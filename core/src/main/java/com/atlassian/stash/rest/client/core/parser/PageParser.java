package com.atlassian.stash.rest.client.core.parser;

import com.atlassian.stash.rest.client.api.entity.Page;
import com.google.common.base.Function;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.List;

import static com.atlassian.stash.rest.client.core.parser.Parsers.listParser;

class PageParser<T> implements Function<JsonElement, Page<T>> {
    final Function<JsonElement, T> valueParser;

    public PageParser(final Function<JsonElement, T> valueParser) {
        this.valueParser = valueParser;
    }

    @Override
    public Page<T> apply(final JsonElement json) {
        JsonObject jsonObject = json.getAsJsonObject();
        List<T> values = listParser(valueParser).apply(jsonObject.getAsJsonArray("values"));

        return new Page<T>(
                jsonObject.get("size").getAsInt(),
                jsonObject.get("limit").getAsInt(),
                jsonObject.has("isLastPage") && jsonObject.get("isLastPage").getAsBoolean(),
                jsonObject.get("start").getAsInt(),
                jsonObject.has("nextPageStart") ? jsonObject.get("nextPageStart").getAsInt() : null,
                values
        );
    }

}
