package com.atlassian.stash.rest.client.core;

import com.atlassian.stash.rest.client.api.StashRestException;
import com.atlassian.stash.rest.client.api.StashUnauthorizedRestException;
import com.atlassian.stash.rest.client.api.entity.Branch;
import com.atlassian.stash.rest.client.api.entity.Page;
import com.atlassian.stash.rest.client.api.entity.Project;
import com.atlassian.stash.rest.client.api.entity.Repository;
import com.atlassian.stash.rest.client.api.entity.UserSshKey;
import com.atlassian.stash.rest.client.core.http.HttpExecutor;
import com.atlassian.stash.rest.client.core.http.HttpRequest;
import com.atlassian.stash.rest.client.core.http.HttpResponse;
import com.atlassian.stash.rest.client.core.http.HttpResponseProcessor;
import com.google.common.collect.ImmutableMap;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.io.ByteArrayInputStream;
import java.util.List;

import static com.atlassian.stash.rest.client.api.EntityMatchers.branch;
import static com.atlassian.stash.rest.client.api.EntityMatchers.page;
import static com.atlassian.stash.rest.client.api.EntityMatchers.project;
import static com.atlassian.stash.rest.client.api.EntityMatchers.repository;
import static com.atlassian.stash.rest.client.api.EntityMatchers.userSshKey;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class StashClientImplTest {
    @Mock
    private HttpExecutor httpExecutor;
    private StashClientImpl stashClient;

    @Before
    public void setUp() throws Exception {
        stashClient = new StashClientImpl(httpExecutor);
    }

    @Test
    public void testGetAccessibleProjects() throws Exception {
        // given
        setHttpResponse(200, TestData.PROJECTS);

        // when
        Page<Project> projectsPage = stashClient.getAccessibleProjects(0, 10);

        // then
        assertThat(projectsPage, page(Project.class)
                        .size(is(1))
                        .limit(is(25))
                        .values(CoreMatchers.<List<Project>>notNullValue())
                        .build()
        );
        assertThat(projectsPage.getValues().get(0), project()
                        .name(is("My Cool Project"))
                        .key(is("PRJ"))
                        .selfUrl(is("http://link/to/project"))
                        .build()
        );
    }

    @Test
    public void testGetRepositories() throws Exception {
        // given
        setHttpResponse(200, TestData.REPOS);

        // when
        Page<Repository> repositoriesPage = stashClient.getRepositories(null, null, 0, 10);

        // then
        assertThat(repositoriesPage, page(Repository.class)
                        .size(is(1))
                        .limit(is(25))
                        .values(CoreMatchers.<List<Repository>>notNullValue())
                        .build()
        );
        assertThat(repositoriesPage.getValues().get(0), repository()
                        .slug(is("my-repo"))
                        .name(is("My repo"))
                        .sshCloneUrl(is("ssh://git@<baseURL>/PRJ/my-repo.git"))
                        .httpCloneUrl(is("https://<baseURL>/scm/PRJ/my-repo.git"))
                        .selfUrl(is("http://link/to/repository"))
                        .project(project()
                                        .name(is("My Cool Project"))
                                        .key(is("PRJ"))
                                        .selfUrl(is("http://link/to/project"))
                                        .build()
                        )
                        .build()
        );
    }

    @Test
    public void testGetRepository() throws Exception {
        // given
        setHttpResponse(200, TestData.REPO_MY_REPO);

        // when
        Repository repository = stashClient.getRepository("PRJ", "my-repo");

        // then
        assertThat(repository, repository()
                        .slug(is("my-repo"))
                        .name(is("My repo"))
                        .sshCloneUrl(is("ssh://git@<baseURL>/PRJ/my-repo.git"))
                        .httpCloneUrl(is("https://<baseURL>/scm/PRJ/my-repo.git"))
                        .selfUrl(is("http://link/to/repository"))
                        .project(project()
                                        .name(is("My Cool Project"))
                                        .key(is("PRJ"))
                                        .selfUrl(is("http://link/to/project"))
                                        .isPublic(is(true))
                                        .isPersonal(is(false))
                                        .build()
                        )
                        .origin(CoreMatchers.<Repository>nullValue())
                        .build()
        );
    }

    @Test
    public void testGetRepositoryPersonalForked() throws Exception {
        // given
        setHttpResponse(200, TestData.REPO_PERSONAL_FORKED);

        // when
        Repository repository = stashClient.getRepository("PRJ", "my-repo");

        // then
        assertThat(repository, repository()
                        .origin(repository()
                                        .name(is("confluence"))
                                        .project(project()
                                                        .name(is("Confluence"))
                                                        .build()
                                        )
                                        .build()
                        )
                        .project(project()
                                        .isPersonal(is(true))
                                        .build()
                        )
                        .build()
        );
    }
    @Test
    public void testGetRepositoryIfNotExists() throws Exception {
        // given
        setHttpResponse(404, "");

        // when
        Repository repository = stashClient.getRepository("PRJ", "NON_EXISTING_REPO");

        // then
        assertThat(repository, nullValue());
    }

    @Test
    public void testGetRepositoryBranches() throws Exception {
        // given
        setHttpResponse(200, TestData.BRANCHES);

        // when
        Page<Branch> branchesPage = stashClient.getRepositoryBranches("PRJ", "my-repo", null, 0, 10);

        // then
        assertThat(branchesPage, page(Branch.class)
                        .size(is(1))
                        .limit(is(25))
                        .values(CoreMatchers.<List<Branch>>notNullValue())
                        .build()
        );
        assertThat(branchesPage.getValues().get(0), branch()
                        .id(is("refs/heads/master"))
                        .displayId(is("master"))
                        .latestChangeset(is("8d51122def5632836d1cb1026e879069e10a1e13"))
                        .isDefault(is(true))
                        .build()
        );
    }

    @Test
    public void testGetRepositoryDefaultBranch() throws Exception {
        // given
        setHttpResponse(200, TestData.BRANCH);

        // when
        Branch branch = stashClient.getRepositoryDefaultBranch("PRJ", "my-repo");

        // then
        assertThat(branch, branch()
                .id(is("refs/heads/master"))
                .displayId(is("master"))
                .latestChangeset(is("8d51122def5632836d1cb1026e879069e10a1e13"))
                .isDefault(is(true))
                .build()
        );
    }

    @Test
    public void testGetUserKeys() throws Exception {
        // given
        setHttpResponse(200, TestData.USER_KEYS);

        // when
        Page<UserSshKey> keysPage = stashClient.getCurrentUserKeys(0, 10);

        // then
        assertThat(keysPage, page(UserSshKey.class)
                        .size(is(2))
                        .limit(is(25))
                        .values(CoreMatchers.<List<UserSshKey>>notNullValue())
                        .build()
        );
        assertThat(keysPage.getValues().get(0), userSshKey()
                        .id(is(1L))
                        .text(is("ssh-rsa AAAAB3... me@127.0.0.1"))
                        .label(is("me@127.0.0.1"))
                        .build()
        );
        assertThat(keysPage.getValues().get(1), userSshKey()
                        .id(is(2L))
                        .text(is("AAAAB3...bo2c="))
                        .label(nullValue(String.class))
                        .build()
        );
    }

    @Test
    public void testIsUserKey() throws Exception {
        // given
        stashClient = new StashClientImpl(httpExecutor, 2); // set page limit to 2 to limit number keys to iterate thru
        // first response
        setHttpResponse(200, TestData.USER_KEYS_2);
        // second response
        setHttpResponse(200, TestData.USER_KEYS);

        // when
        // check key from seconds response
        boolean isKey = stashClient.isUserKey("ssh-rsa AAAAB3... me@127.0.0.1");

        // then
        assertThat(isKey, is(true));
    }

    @Test(expected = StashRestException.class)
    public void testHttpErrorCausesStashRestException() throws Exception {
        // given
        setHttpResponse(400, "");

        // when
        stashClient.getStashApplicationProperties();
    }

    @Test(expected = StashUnauthorizedRestException.class)
    public void testHttpError401CausesStashUnauthorizedRestException() throws Exception {
        // given
        setHttpResponse(401, "");

        // when
        stashClient.getStashApplicationProperties();
    }

    private void setHttpResponse(final int statusCode, final String body) {
        Mockito.doAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                HttpResponseProcessor responseProcessor = (HttpResponseProcessor) invocation.getArguments()[1];
                HttpResponse response = new HttpResponse(statusCode, "" + statusCode,
                        ImmutableMap.<String, String>of(), new ByteArrayInputStream(body.getBytes("UTF-8")));
                return responseProcessor.process(response);
            }
        }).when(httpExecutor).execute(Mockito.any(HttpRequest.class), Mockito.any(HttpResponseProcessor.class));
    }
}
