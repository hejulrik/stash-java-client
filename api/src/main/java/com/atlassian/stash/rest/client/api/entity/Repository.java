package com.atlassian.stash.rest.client.api.entity;

import com.google.common.base.Objects;
import com.google.common.base.Strings;

import javax.annotation.Nullable;

/**
 * Describes a repository
 */
public class Repository {
    private final String slug;
    private final long id;
    private final String name;
    private final boolean isPublic;
    private final String sshCloneUrl;
    private final String httpCloneUrl;
    private final String selfUrl;
    private final Project project;
    private final Repository origin;

    public Repository(final String slug, final long id, final String name, final boolean isPublic,
                      final String sshCloneUrl, final String httpCloneUrl, final String selfUrl,
                      final Project project, final Repository origin) {
        this.slug = slug;
        this.id = id;
        this.name = name;
        this.isPublic = isPublic;
        this.sshCloneUrl = sshCloneUrl;
        this.httpCloneUrl = httpCloneUrl;
        this.selfUrl = selfUrl;
        this.project = project;
        this.origin = origin;
    }

    /**
     * Retrieves the "slug" for this repository, which is a URL-friendly variant of its {@link #getName() name}. Each
     * repository's slug is guaranteed to be unique within its {@link #getProject() project}, but <i>not</i> within
     * the system at large.
     *
     * @return the repository's slug
     */
    public String getSlug() {
        return slug;
    }

    /**
     * Retrieves the repository's ID, which represents its primary key.
     *
     * @return the repository's ID
     */
    public long getId() {
        return id;
    }

    /**
     * Retrieves the repository's name, which is guaranteed to be unique within its {@link #getProject() project} but
     * <i>not</i> within the system at large.
     *
     * @return the repository's name
     */
    public String getName() {
        return name;
    }

    /**
     * Retrieves a flag indicating whether this repository is public.
     * <p/>
     * Note, this flag is taken into account when calculating whether this repository is accessible to
     * unauthenticated users but is <i>not</i> the definitive answer.
     *
     * @return {@code true} if the repository has been marked as public, {@code false} otherwise
     */
    public boolean isPublic() {
        return isPublic;
    }

    public boolean hasCloneUrl() {
        return !Strings.isNullOrEmpty(getHttpCloneUrl()) || !Strings.isNullOrEmpty(getSshCloneUrl());
    }

    /**
     * @return Repository SSH clone url
     */
    public String getSshCloneUrl() {
        return sshCloneUrl;
    }

    /**
     * @return Repository HTTP/HTTPS clone url
     */
    public String getHttpCloneUrl() {
        return httpCloneUrl;
    }

    /**
     * @return Repository UI web page url
     */
    public String getSelfUrl() {
        return selfUrl;
    }

    /**
     * @return the project to which this repository belongs
     */
    public Project getProject() {
        return project;
    }

    /**
     * @return Origin repository for the repository if it was forked. {@code null} otherwise.
     */
    @Nullable
    public Repository getOrigin() {
        return origin;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("slug", slug)
                .add("id", id)
                .add("name", name)
                .add("public", isPublic)
                .add("sshCloneUrl", sshCloneUrl)
                .add("httpCloneUrl", httpCloneUrl)
                .add("selfUrl", selfUrl)
                .add("project", project)
                .add("origin", origin)
                .toString();
    }
}
