package com.atlassian.stash.rest.client.api;

import java.util.List;

/**
 * Exception caused by HTTP 401 Unauthorized
 */
public class StashUnauthorizedRestException extends StashRestException {

    public StashUnauthorizedRestException(String message, int statusCode, String statusMessage) {
        super(message, statusCode, statusMessage);
    }

    public StashUnauthorizedRestException(List<StashError> errors, int statusCode, String statusMessage) {
        super(errors, statusCode, statusMessage);
    }

    public StashUnauthorizedRestException(List<StashError> errors, int statusCode, String statusMessage, String responseBody) {
        super(errors, statusCode, statusMessage, responseBody);
    }
}
