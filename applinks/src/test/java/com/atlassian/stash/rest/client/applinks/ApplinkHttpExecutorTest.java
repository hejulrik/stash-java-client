package com.atlassian.stash.rest.client.applinks;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.ApplicationLinkResponseHandler;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response;
import com.atlassian.stash.rest.client.core.http.HttpMethod;
import com.atlassian.stash.rest.client.core.http.HttpRequest;
import com.atlassian.stash.rest.client.core.http.HttpResponseProcessor;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.net.URI;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.sameInstance;

@RunWith(MockitoJUnitRunner.class)
public class ApplinkHttpExecutorTest {
    @Mock private ApplicationLink applicationLink;
    @Mock private ApplicationLinkRequestFactory applicationLinkRequestFactory;
    @Mock private ApplicationLinkRequest applicationLinkRequest;
    private ApplinkHttpExecutor httpExecutor;

    @Before
    public void setUp() throws Exception {
        Mockito.when(applicationLink.createAuthenticatedRequestFactory()).thenReturn(applicationLinkRequestFactory);
        Mockito.when(applicationLinkRequestFactory.getAuthorisationURI()).thenReturn(URI.create("http://site.com/authorise"));
        httpExecutor = new ApplinkHttpExecutor(applicationLink);
    }

    @Test
    public void testExecuteHandlesCredentialsRequiredThrownFromFactory() throws Exception {
        HttpResponseProcessor<?> processor = Mockito.mock(HttpResponseProcessor.class);
        Mockito.when(applicationLinkRequestFactory.createRequest(Mockito.any(Request.MethodType.class), Mockito.anyString()))
                .thenThrow(new CredentialsRequiredException(applicationLinkRequestFactory, "credentials required"));

        try {
            httpExecutor.execute(new HttpRequest("/some", HttpMethod.GET, null, false), processor);
            Assert.fail("StashCredentialsRequiredException should be thrown");
        } catch (StashCredentialsRequiredException e) {
            assertValidStashCredentialsRequiredException(e);
        }

    }

    @Test
    public void testExecuteHandlesCredentialsRequiredThrownFromRequest() throws Exception {
        HttpResponseProcessor<?> processor = Mockito.mock(HttpResponseProcessor.class);
        Mockito.when(applicationLinkRequestFactory.createRequest(Mockito.any(Request.MethodType.class), Mockito.anyString()))
                .thenReturn(applicationLinkRequest);
        Mockito.when(applicationLinkRequest.execute(Mockito.any(ApplicationLinkResponseHandler.class)))
                .thenAnswer(new Answer<Object>() {
                    @Override
                    public Object answer(InvocationOnMock invocation) throws Throwable {
                        ApplicationLinkResponseHandler handler = (ApplicationLinkResponseHandler) invocation.getArguments()[0];
                        handler.credentialsRequired(Mockito.mock(Response.class));
                        return null;
                    }
                });

        try {
            httpExecutor.execute(new HttpRequest("/some", HttpMethod.GET, null, false), processor);
            Assert.fail("StashCredentialsRequiredException should be thrown");
        } catch (StashCredentialsRequiredException e) {
            assertValidStashCredentialsRequiredException(e);
        }

    }

    private void assertValidStashCredentialsRequiredException(StashCredentialsRequiredException e) {
        Assert.assertThat(e.getApplicationLink(), sameInstance(applicationLink));
        Assert.assertThat(e.getCause(), notNullValue());
        Assert.assertThat(e.getCause().getAuthorisationURI(), notNullValue());
    }
}
