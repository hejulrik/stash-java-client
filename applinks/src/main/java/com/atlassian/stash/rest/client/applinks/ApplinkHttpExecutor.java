package com.atlassian.stash.rest.client.applinks;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.ApplicationLinkResponseHandler;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.auth.Anonymous;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.stash.rest.client.api.StashException;
import com.atlassian.stash.rest.client.core.http.HttpExecutor;
import com.atlassian.stash.rest.client.core.http.HttpRequest;
import com.atlassian.stash.rest.client.core.http.HttpResponse;
import com.atlassian.stash.rest.client.core.http.HttpResponseProcessor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static com.atlassian.stash.rest.client.core.StashRestClientProperties.STASH_REST_SOCKET_TIMEOUT;

public class ApplinkHttpExecutor implements HttpExecutor {
    @Nonnull private final ApplicationLink applicationLink;
    @Nonnull private final ApplicationLinkRequestFactory anonymousRequestFactory;
    @Nonnull private final ApplicationLinkRequestFactory authenticatedRequestFactory;


    public ApplinkHttpExecutor(@Nonnull ApplicationLink applicationLink) {
        this.applicationLink = applicationLink;
        this.anonymousRequestFactory = applicationLink.createAuthenticatedRequestFactory(Anonymous.class);
        this.authenticatedRequestFactory = applicationLink.createAuthenticatedRequestFactory();
    }

    @Nullable
    @Override
    public <T> T execute(@Nonnull HttpRequest httpRequest, @Nonnull HttpResponseProcessor<T> responseProcessor) throws StashException {
        final ApplicationLinkRequestFactory requestFactory =
                (httpRequest.isAnonymous() ? anonymousRequestFactory : authenticatedRequestFactory);

        String requestUrl = httpRequest.getUrl();
        Request.MethodType methodType = Request.MethodType.valueOf(httpRequest.getMethod().name());

        try {
            final ApplicationLinkRequest request = requestFactory.createRequest(methodType, requestUrl);
            request.setRequestContentType("application/json");
            if (methodType != Request.MethodType.GET && httpRequest.getPayload() != null) {
                request.setRequestBody(httpRequest.getPayload());
            }
            request.setSoTimeout((int) TimeUnit.SECONDS.toMillis(STASH_REST_SOCKET_TIMEOUT));

            return request.execute(new StashApplinkResponseHandler<T>(requestFactory, applicationLink, responseProcessor));
        } catch (ResponseException e) {
            throw new StashException(e);
        } catch (CredentialsRequiredException e) {
            throw new StashCredentialsRequiredException(e, applicationLink);
        }
    }

    private static class StashApplinkResponseHandler<T> implements ApplicationLinkResponseHandler<T> {
        @Nonnull private final ApplicationLinkRequestFactory applinkRequestFactory;
        @Nonnull private final ApplicationLink applink;
        @Nonnull private final HttpResponseProcessor<T> responseProcessor;

        private StashApplinkResponseHandler(@Nonnull ApplicationLinkRequestFactory applinkRequestFactory,
                                            @Nonnull ApplicationLink applink,
                                            @Nonnull HttpResponseProcessor<T> responseProcessor) {
            this.applinkRequestFactory = applinkRequestFactory;
            this.applink = applink;
            this.responseProcessor = responseProcessor;
        }

        @Override
        public T credentialsRequired(Response response) throws ResponseException {
            throw new StashCredentialsRequiredException(
                    new CredentialsRequiredException(applinkRequestFactory,
                            "You do not have an authorized access token for the remote resource."), applink
            );
        }

        public T handle(Response response) {
            try {
                HttpResponse coreResponse = new HttpResponse(response.getStatusCode(), response.getStatusText(),
                        response.getHeaders(), response.getResponseBodyAsStream());
                return responseProcessor.process(coreResponse);
            } catch (IOException e) {
                throw new StashException(e);
            } catch (ResponseException e) {
                throw new StashException(e);
            }
        }
    }
}
