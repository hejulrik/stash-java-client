<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>com.atlassian.pom</groupId>
        <artifactId>public-pom</artifactId>
        <version>3.0.7</version>
    </parent>

    <groupId>com.atlassian.stash</groupId>
    <artifactId>stash-java-client</artifactId>
    <version>1.3.10-SNAPSHOT</version>
    <packaging>pom</packaging>
    <name>stash-java-client</name>
    <description>Stash client using REST API through Application Links or Apache HttpClient</description>
    <organization>
        <name>Atlassian</name>
        <url>http://www.atlassian.com</url>
    </organization>

    <issueManagement>
        <system>Bitbucket</system>
        <url>https://bitbucket.org/atlassian/stash-java-client/issues</url>
    </issueManagement>

    <licenses>
        <license>
            <name>Apache License, Version 2.0</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0</url>
            <distribution>repo</distribution>
        </license>
    </licenses>
                                       
    <scm>
        <connection>scm:git:ssh://git@bitbucket.org/atlassianlabs/stash-java-client.git</connection>
        <developerConnection>scm:git:ssh://git@bitbucket.org/atlassianlabs/stash-java-client.git</developerConnection>
        <url>https://bitbucket.org/atlassianlabs/stash-java-client</url>
      <tag>HEAD</tag>
  </scm>

    <modules>
        <module>api</module>
        <module>core</module>
        <module>applinks</module>
        <module>httpclient</module>
        <module>tests</module>
    </modules>


    <dependencyManagement>
        <dependencies>

            <dependency>
                <groupId>com.google.guava</groupId>
                <artifactId>guava</artifactId>
                <version>${guava.version}</version>
            </dependency>

            <dependency>
                <groupId>com.atlassian.applinks</groupId>
                <artifactId>applinks-api</artifactId>
                <version>${applinks.version}</version>
            </dependency>

            <dependency>
                <groupId>com.atlassian.applinks</groupId>
                <artifactId>applinks-spi</artifactId>
                <version>${applinks.version}</version>
            </dependency>

            <dependency>
                <groupId>com.google.code.gson</groupId>
                <artifactId>gson</artifactId>
                <version>2.2.4</version>
            </dependency>

            <dependency>
                <groupId>junit</groupId>
                <artifactId>junit</artifactId>
                <version>${junit.version}</version>
            </dependency>

            <dependency>
                <groupId>org.hamcrest</groupId>
                <artifactId>hamcrest-core</artifactId>
                <version>${hamcrest.version}</version>
            </dependency>

            <dependency>
                <groupId>com.atlassian.sal</groupId>
                <artifactId>sal-api</artifactId>
                <version>2.10.11</version>
            </dependency>

            <dependency>
                <groupId>log4j</groupId>
                <artifactId>log4j</artifactId>
                <version>${log4j.version}</version>
            </dependency>

        </dependencies>
    </dependencyManagement>

    <profiles>
        <profile>
            <id>checks</id>
            <activation>
                <activeByDefault>true</activeByDefault>
                <property>
                    <name>skipChecks</name>
                    <value>false</value>
                </property>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-checkstyle-plugin</artifactId>
                        <version>2.12</version>
                        <configuration>
                            <configLocation>checkstyle.xml</configLocation>
                            <encoding>UTF-8</encoding>
                            <consoleOutput>true</consoleOutput>
                            <failsOnError>true</failsOnError>
                            <linkXRef>false</linkXRef>
                        </configuration>
                        <executions>
                            <execution>
                                <id>checkstyle</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>check</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>org.codehaus.mojo</groupId>
                        <artifactId>findbugs-maven-plugin</artifactId>
                        <version>2.5.3</version>
                        <configuration>
                            <includeFilterFile>findbugs-include.xml</includeFilterFile>
                            <includeTests>true</includeTests>
                        </configuration>
                        <executions>
                            <execution>
                                <id>findbugs</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>check</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
        <profile>
            <id>skipChecks</id>
            <activation>
                <property>
                    <name>skipChecks</name>
                </property>
            </activation>
        </profile>
    </profiles>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>com.atlassian.maven.plugins</groupId>
                    <artifactId>maven-amps-plugin</artifactId>
                    <version>${amps.version}</version>
                </plugin>
                <plugin>
                    <groupId>com.atlassian.maven.plugins</groupId>
                    <artifactId>maven-amps-dispatcher-plugin</artifactId>
                    <version>${amps.version}</version>
                </plugin>
            </plugins>
        </pluginManagement>

        <plugins>
            <plugin>
                <artifactId>maven-surefire-plugin</artifactId>
                <configuration>
                    <excludes>
                        <exclude>it/**</exclude>
                    </excludes>
                </configuration>
            </plugin>
        </plugins>

    </build>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <guava.version>10.0.1</guava.version>
        <applinks.version>4.1.1</applinks.version>
        <findbugs.version>1.3.7</findbugs.version>
        <amps.version>4.2.18</amps.version>
        <plugin.testrunner.version>1.1.4</plugin.testrunner.version>
        <stash.version>2.11.3</stash.version>
        <stash.port>7990</stash.port>
        <bamboo.version>5.4.2</bamboo.version>
        <bamboo.data.version>3.2.2</bamboo.data.version>
        <bamboo.port>6990</bamboo.port>
        <bamboo.jvmargs />
        <stash.jvmargs />
        <junit.version>4.10</junit.version>
        <hamcrest.version>1.1</hamcrest.version>
        <log4j.version>1.2.16</log4j.version>
    </properties>

</project>
